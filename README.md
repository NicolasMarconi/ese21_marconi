# ese21_nicomarconi

## Proyecto "Monitor de Arrtimias Ventriculares"
### Objetivo:
     Diseñar y desarrollar un sistema embebido que sea capáz de **detectar y notificar** mediante puerto 
      serie, arritmias ventriculares graves, tales como fibrilación ventricular, taquicardias ventriculares,
      etc.. 
      

## Diagrama de Bloques del Proyecto
![Diagrama](Diagrama_de_Bloques_Proyecto.png)

## Listado de Materiales:

   * EDU-CIAA
   * Kit de Desarrollo ADS 1298
   * Computadora
   * Simulador de ECG de paciente
   



    
